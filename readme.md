# OfflinePay v0.0.1
**Requires Vault.**
*Essentials* enables some extra functionality (like sending of mails) but other economy plugins might work as well.
Not tested, however.

This plugin allows its users to pay offline players with the `/offlinepay` command.
It also allows paying offline (or online) users periodically with the `/periodicpay` command.
This can be used as scheduled rent payment, for instance.

The plugin comes with an array of other commands for listing active (as well as inactive, old) payments and cancelling existing commands.
There are also a number of configurable parameters in `config.yml` as well as a customizable `messages.yml`.
Tab completion works for offline players unless disabled in the config.

### Commands and permissions
```
Command             	Permission              	Description                             
offlinepay          	offlinepay.offlinepay   	Pay an offline player                   
periodicpay         	offlinepay.periodicpay  	Pay a player periodically               
mypayments          	offlinepay.payments     	See your active periodic payments       
oldpayments         	offlinepay.payments.old 	See old periodic payments of a player   
paymentstome        	offlinepay.paymentstome 	See active periodic payments to me      
oldpaymentstome     	offlinepay.paymentstome.old	See old periodic payments to me         
cancelpayment       	offlinepay.cancel       	Cancel your payment to a player         
cancelotherpayment  	offlinepay.cancel.others	Cancel your payment to a player         
offlinepayreload    	offlinepay.reload       	Reload the config                       

```

### Extra permissions
```
Permission              	Children                                        	Descrpition                             
offlinepay.user         	offlinepay.offlinepay, offlinepay.periodicpay,  	Allows using the commands for regular users
                        	offlinepay.payments, offlinepay.paymentstome,   	                                        
                        	offlinepay.cancel                               	                                        
offlinepay.*            	offlinepay.offlinepay, offlinepay.periodicpay,  	Allows using all OfflinePay commands    
                        	offlinepay.payments, offlinepay.payments.others, 	                                        
                        	offlinepay.payments.all, offlinepay.payments.old, 	                                        
                        	offlinepay.paymentstome, offlinepay.paymentstome.others, 	                                        
                        	offlinepay.paymentstome.old, offlinepay.cancel, 	                                        
                        	offlinepay.cancel.others                        	                                        
offlinepay.offlinepay   	None                                            	Allow paying offline players            
offlinepay.periodicpay  	None                                            	Allow paying other players periodically 
offlinepay.reload       	None                                            	Allow reloading the config              
offlinepay.payments     	None                                            	Allow seeing your own active periodic payments
offlinepay.payments.others	None                                            	Allow seeing active periodic payments of others
offlinepay.payments.all 	None                                            	Allow seeing all active periodic payments
offlinepay.payments.old 	None                                            	Allow seeing of old periodic payments   
offlinepay.paymentstome 	None                                            	Allow seeing active periodic payments to you
offlinepay.paymentstome.others	None                                            	Allow seeing active periodic payments to others
offlinepay.paymentstome.old	None                                            	Allow seeing old periodic payments to you
offlinepay.cancel       	None                                            	Allow cancelling your periodic payments 
offlinepay.cancel.others	None                                            	Allow cancelling your periodic payments of others
```

### Config explanation
```
money:
  minpay: 0.01							# the minimum amount that can be paid
  symbol: '$'                   	# the symbol used for money
periodicpay:
  try-again-time: 120					# the time (in seconds) after which a failed payment will be retried
  send-mail-on-failure: true			# whether or not to send a mail to both parties when a payment failes (requires Essentials)
  min-delay: 10						# minimum delay of periodic payment (in seconds)
send-mail-to-offline: true			# whether or not to send mail to players who are offline while receiving money (requires Essentials)
log-payments-to-console: true		# whether or not to send information to the console when a payment is made
enable-tab-complete: true			# whether or not offline player tab completion is enabled for commands
```
