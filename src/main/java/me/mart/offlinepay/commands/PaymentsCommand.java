package me.mart.offlinepay.commands;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.OfflinePlayer;

import com.google.common.collect.Lists;

import me.mart.offlinepay.OfflinePay;
import me.mart.offlinepay.PeriodicPayment;
import me.mart.offlinepay.utils.Files;
import me.mart.offlinepay.utils.Messages;

/**
 * Class for commands to check payments - /mypayments and /oldpayments fall here
 * 
 * @author mart
 *
 */
public class PaymentsCommand extends NormalOfflinePayCommand {
	public static PaymentsCommand instance = new PaymentsCommand();
	private final OfflinePay op = OfflinePay.getOfflinePay();

	/**
	 * Overrides the abstract method exectued when command is used
	 */
	@Override
	protected boolean doCommand() {
		boolean old = command.getName().toLowerCase().startsWith("old"); // check if /oldpayments or /mypayments
		boolean tome = command.getName().toLowerCase().endsWith("tome"); // check if /paymentstome or /oldpaymentstome
																			// instead of /payments or /oldpayments
		return getPayments(old, tome);
	}

	/**
	 * Shows payments to sender. Works for both new and old payments as well as
	 * payments made to someone and by someone
	 * 
	 * @param old  whether or not looking at old payments
	 * @param tome whether or not looking for player TO someone
	 * @return true unless invalid arguments
	 */
	protected boolean getPayments(boolean old, boolean tome) {
		int page = getPage();
		op.getServer().getScheduler().runTaskAsynchronously(op, new Runnable() {
			boolean all = false;

			@Override
			public void run() {
				if (sender.hasPermission("offlinepay.payments.all") && args.length > 0
						&& (args[0].equalsIgnoreCase("all") || args[0].equalsIgnoreCase("*"))) {
					all = true;
				}
				OfflinePlayer player;
				if (!all) { // canOther? needOther=false
					if (!tome) {
						player = getOfflinePlayer(sender.hasPermission("offlinepay.payments.others"), false);
					} else { // different perms for paymentstome
						player = getOfflinePlayer(sender.hasPermission("offlinepay.paymentstome.others"), false);
					}
					if (player == null) { // not found + dealt with
						return;
					}
				} else {
					player = null;
				}
				op.getServer().getScheduler().runTask(op, () -> {
					Map<String, String> map = new HashMap<String, String>();
					if (all) {
						map.put("\\{player\\}", "all players");
					} else {
						map.put("\\{player\\}", player.getName());
					}
					List<PeriodicPayment> pmnts = OfflinePay.getOfflinePay().getPayments();
					if (old) {
						pmnts = Lists.reverse(Files.loadPeriodicPayments(true)); // oldest last
					}
					map.put("\\{active\\}", old ? "Old" : "Active");
					map.put("\\{tofrom\\}", tome ? "to" : "of");
					map.put("\\{page\\}", "" + page);
					sender.sendMessage(Messages.START_PERIODICPAY_LIST.get(map));
					boolean found = false;
					int i = 0;
					for (PeriodicPayment pmnt : pmnts) {
						if (all || (!tome && pmnt.getFromUUID().equals(player.getUniqueId()))
								|| (tome && pmnt.getToUUID().equals(player.getUniqueId()))) {
							if (i >= (page - 1) * 4 && i < page * 4) { // 4 per page
								map = pmnt.getMap();
								if (!old) {
									sender.sendMessage(Messages.PAYMENT_DESCRIPTION.get(map));
								} else {
									sender.sendMessage(Messages.PAYMENT_DESCRIPTION_OLD.get(map));
								}
								found = true;
							}
							i++;
						}
					}
					if (!found) {
						if (!old) {
							sender.sendMessage(Messages.NO_ACTIVE_PAYMENTS.get());
						} else {
							sender.sendMessage(Messages.NO_OLD_PAYMENTS.get());
						}
					}
				});
			}

		});
		return true;
	}

	/**
	 * Get the page number from the last argument
	 * 
	 * @return the page number
	 */
	protected int getPage() {
		if (args.length == 0) {
			return 1;
		} else {
			try {
				return Integer.parseInt(args[args.length - 1]);
			} catch (NumberFormatException e) {
				return 1;
			}
		}
	}

}
