package me.mart.offlinepay.commands;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import me.mart.offlinepay.OfflinePay;
import me.mart.offlinepay.PeriodicPayment;
import me.mart.offlinepay.Scheduler;
import me.mart.offlinepay.utils.Files;
import me.mart.offlinepay.utils.Messages;
import me.mart.offlinepay.utils.Utils;

/**
 * Handles the /periodicpay <offlineplayer> <time> <amount> <repeat> command 
 * 
 * @author mart
 *
 */
public class PeriodicPayCommand extends NonConsoleOfflinePayCommand {
	public static PeriodicPayCommand instance = new PeriodicPayCommand();

	/**
	 * Overrides the abstract class run when command is used
	 * Creates a new Periodic Payment if possible
	 */
	@Override
	protected boolean doCommand() {
		// /<command> <player> <period> <amount> <repeats|inf>
		if (args.length < 4) {
			return false;
		}
		OfflinePay.getOfflinePay().getServer().getScheduler().runTaskAsynchronously(OfflinePay.getOfflinePay(), 
				new Runnable() {

					@Override
					public void run() {
						OfflinePlayer other = getOfflinePlayer();
						if (other == null) { // problem + already notified
							return; 
						}
						String timeperiod = args[1];
						String amountstring = args[2].replaceAll("[^0-9\\.]", "");
						String repeatstring = args[3];
						parseAmount(amountstring);
						if (amount == -1L) { // problem + already reported
							return;
						}
						setDelay(timeperiod);
						if (delay == -1L) { // problem + already reported
							return;
						}
						OfflinePay.LOGGER.info(timeperiod + "->" + delay);
						setRepeats(repeatstring);
						if (repeats == -2L) {// problem + already reported
							return;
						}
						Player payfrom = (Player) sender;
						PeriodicPayment pmnt = new PeriodicPayment(payfrom, other, amount, delay, repeats);
						OfflinePay.getOfflinePay().addPayment(pmnt);
						Scheduler.schedulePeriodicPayment(pmnt);
						Files.addPeriodicPayment(pmnt);
						Map<String, String> map = new HashMap<String, String>();
						map.put("\\{to\\}", other.getName());
						map.put("\\{amount\\}", OfflinePay.getOfflinePay().getMoneySymbol() + pmnt.getAmount());
						map.put("\\{delay\\}", Utils.formatDateFromDiff(pmnt.getDelay()*1000L));
						map.put("\\{repeats\\}", String.valueOf(pmnt.getRepeats()));
						sender.sendMessage(Messages.START_PERIODICPAY.get(map));
						if (OfflinePay.getOfflinePay().logPaymentsToConsole) {
							OfflinePay.LOGGER.info(Messages.START_PERIODICPAY.get(map));
						}
					}
			
		});
		return true;
	}

}
