package me.mart.offlinepay.commands;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import me.mart.offlinepay.OfflinePay;
import me.mart.offlinepay.exceptions.ConsoleCannotUseCommandException;
import me.mart.offlinepay.exceptions.PlayerNotFoundException;
import me.mart.offlinepay.utils.Messages;
import me.mart.offlinepay.utils.Utils;

/**
 * Base class for OfflinePay commands 
 * 
 * @author mart
 *
 */
public abstract class OfflinePayCommandBase implements CommandExecutor, TabCompleter {
	protected CommandSender sender;
	protected Command command;
	protected String[] args;
	protected long delay = -1L; // for those who need it
	protected double amount = -1; // for those who need it
	protected long repeats = -2L; // for thsoe who need it
	
	/** 
	 * Overriding default command to use doCommand and set some initial variables;
	 * 
	 * @param csender    sender of the command
	 * @param cmd        command
	 * @param label      alias of command used
	 * @param cargs      command arguments
	 */
	@Override
	public boolean onCommand(CommandSender csender, Command cmd, String label, String[] cargs) {
		delay = -1L;
		amount = -1;
		repeats = -2L;
		sender = csender;
		command = cmd;
		args = cargs;
		return doCommand();
	}
	
	/**
	 * Abstract method that base classes need to override
	 * 
	 * @return  false if invalid arguments, true otherwise
	 */
	protected abstract boolean doCommand();
	
	
	
	
	/**
	 * Default tab completion returns list of OfflinePlayers for the first argument and the default tabcompletion otherwise
	 * 
	 */
	public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
		if (!OfflinePay.getOfflinePay().getEnableTabComplete()) {
			return OfflinePay.getOfflinePay().onTabComplete(sender, command, label, args);
		}
		if (args.length == 1) { // default to 2nd argument being an offline player
			return getMatchingOfflinePlayers(args[0]);
		}
		return OfflinePay.getOfflinePay().onTabComplete(sender, command, label, args);
	}
	
	/**
	 * Finds offline player names that match the start of the string already typed
	 * 
	 * @param match  matching string looked for
	 * @return  	 list of matching offline player names
	 */
	private List<String> getMatchingOfflinePlayers(String match){
		List<String> list = new ArrayList<String>();
		for (OfflinePlayer op : Bukkit.getOfflinePlayers()) {
			if (op.getName().toLowerCase().startsWith(match.toLowerCase())) {
				list.add(op.getName());
			}
		}
		return list;
	}

	/**
	 * Get an offline player from arguments given a command sender and 
	 * whether or not the sender has permissions to get other's information
	 * or is console. 
	 * 
	 * @param canOthers  if sender can get information about others
	 * @param needOther  if another player must necessarily be returned
	 * @param consoleCan if the console can use this command
	 * @return           an existing offline player if one exists
	 * @throws ConsoleCannotUseCommandException   if the console is not allowed to use the command
	 * @throws PlayerNotFoundException            if player not found yet needed
	 */
	protected OfflinePlayer getOfflinePlayer(boolean canOthers, boolean needOther,
					boolean consoleCan) throws PlayerNotFoundException, ConsoleCannotUseCommandException {
		boolean isPlayer = sender instanceof Player;
		OfflinePlayer player = null;
		if (!isPlayer) { // sender is not player (console or command block)
			if (consoleCan) { // console can use this command
				if (args.length > 0) { // there is an argument (should already have been tested for)
					player = OfflinePay.getOfflinePay().getOfflinePlayerFromArg(args[0]); // throws exception if not found
				} else { // console needs to provide arguments for this command
					throw new PlayerNotFoundException();
				}
			} else { // console cannot use this command
				throw new ConsoleCannotUseCommandException();
			}
		} else { // sender is Player
			player = (OfflinePlayer) sender;
			if (canOthers) { // sender can see other's information
				if (args.length > 0) { // therei s an argument
					try { // try and get a player
						player = OfflinePay.getOfflinePay().getOfflinePlayerFromArg(args[0]);
					} catch (PlayerNotFoundException e) { // none exist -> default to sender
						if (needOther) {
							throw e;
						}
					}
				} // if not canOther -> defaults to sender
			} 
		}
		return player;
	}
	
	/**
	 * Parses the amount as given on command argument into a double
	 * and handles all the problems along the way
	 * 
	 * @param amountstring string read from arguments
	 * @return
	 */
	protected double parseAmount(String amountstring) {
		if (amountstring.contains("-")) {
            sender.sendMessage(Messages.AMOUNT_MUST_BE_POSITIVE.get());
            amount = -1;
			return -1;
        }
		amountstring = amountstring.replaceAll("[^0-9\\.]", "");
		if (amountstring.isEmpty()) { // cannot process amount
			sender.sendMessage(Messages.PROVIDE_AMOUNT.get());
            amount = -1;
			return -1;
		}
		try {
			amount = new Double(amountstring);
		} catch (NumberFormatException e) {
            sender.sendMessage(Messages.PROVIDE_AMOUNT.get());
            amount = -1;
			return -1;
		}
		double minPay = OfflinePay.getOfflinePay().getMinPay();
		String ms = OfflinePay.getOfflinePay().getMoneySymbol();
		if (amount < minPay) { // Check if amount is less than minimum-pay-amount
			Map<String, String> map = new HashMap<String, String>();
			map.put("\\{amount\\}", String.valueOf(ms + minPay));
            sender.sendMessage(Messages.MINIMUM_PAYMENT.get(map));
            amount = -1;
			return -1;
        }
		return amount;
	}
	
	/**
	 * Set the protected member delay (and also return it)
	 * This version tells the sender to correct it in case of problems
	 * 
	 * @param str  string to convert into a delay
	 * @return     the delay in seconds if successful, -1L otherwise
	 */
	protected double setDelay(String str) {
		return setDelay(str, true);
	}

	/**
	 * Set the protected member delay (and also return it)
	 * 
	 * @param str   string to convert into a delay
	 * @param bShow whether or not to tell the sender what the problem is right away
	 * @return      the delay in seconds if successful, -1L otherwise
	 */
	protected double setDelay(String str, boolean bShow) {
		if (str.startsWith(".")) {
			str = "0" + str; // otherwise it reads .001 -> 1
		}
		delay = -1L;
		try {
			delay = Utils.parseDateDiff(str)/1000L; // in seconds
		} catch (Exception e){
			if (bShow) {
				sender.sendMessage(Messages.PROVIDE_TIME.get());
				delay = -1L;
				return -1L;
			}
		}
		if (delay < OfflinePay.getOfflinePay().getMinDelay()) {
			if (bShow) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("\\{mindelay\\}", Utils.formatDateFromDiff(OfflinePay.getOfflinePay().getMinDelay()*1000L));
				sender.sendMessage(Messages.MINIMUM_DELAY.get(map));
			}
			delay = -1L;
			return -1L;
		}
		return delay;
	}
	
	/**
	 * Set the protected member repeats (and also return it)
	 * 
	 * @param str  string to be turned into a long
	 * @return     nr of repeats if successful, -2L if not, -1L if infinitely many
	 */
	protected long setRepeats(String str) {
		repeats = -2L;
		if (str.equalsIgnoreCase("inf")) {
			repeats = -1L;
		} else {
			try {
				repeats = Long.parseLong(str);
			} catch (NumberFormatException e) {
				sender.sendMessage(Messages.PROVIDE_REPEATS.get());
			}
		}
		return repeats;
		
	}

}
