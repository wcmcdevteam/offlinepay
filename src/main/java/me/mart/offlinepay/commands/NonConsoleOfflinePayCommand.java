package me.mart.offlinepay.commands;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.OfflinePlayer;

import me.mart.offlinepay.exceptions.ConsoleCannotUseCommandException;
import me.mart.offlinepay.exceptions.PlayerNotFoundException;
import me.mart.offlinepay.utils.Messages;

/**
 * A command that console should not be using - the /offlinepay and /periodicpay commands fall here
 * 
 * @author mart
 *
 */
public abstract class NonConsoleOfflinePayCommand extends OfflinePayCommandBase {
	
	
	/**
	 * Find an OfflinePlayer, if one couldn't be found, send the appropriate response and return null
	 * 
	 * @return OfflinePlayer if one exists, null otherwise
	 */
	protected OfflinePlayer getOfflinePlayer() {
		OfflinePlayer other = null;
		try {               // canOther=true, needOther=true,consoleCan=false
			other = super.getOfflinePlayer(true, true, false); 
		} catch (ConsoleCannotUseCommandException e) {
			sender.sendMessage(Messages.CONSOLE_OFFLINEPAY.get()); // console goes here
			return null;
		} catch (PlayerNotFoundException e) {
			Map<String, String> map = new HashMap<String, String>();
			map.put("\\{player\\}", args[0]);
			sender.sendMessage(Messages.PLAYER_NOT_FOUND.get(map));
			return null;
		}
		if (sender.getName().equalsIgnoreCase(other.getName())) {
			sender.sendMessage(Messages.CANNOT_PAY_SELF.get());
			return null;
		}
		return other; // null if all else fails
	}

}
