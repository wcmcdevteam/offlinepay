package me.mart.offlinepay.commands;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import static java.util.stream.Collectors.joining;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import me.mart.offlinepay.OfflinePay;
import me.mart.offlinepay.PeriodicPayment;
import me.mart.offlinepay.exceptions.PlayerNotFoundException;
import me.mart.offlinepay.utils.Messages;
import me.mart.offlinepay.utils.Utils;

/**
 * Handles the /cancelpayment and /cancelotherpayment commands /cancelpayment
 * [toplayer] [amount] [delay] [repeats] [nr] /cancelotherpayment <fromplayer>
 * [toplayer] [amount] [delay] [repeats] [nr]
 * 
 * @author mart
 *
 */
public class CancelPaymentCommand extends NormalOfflinePayCommand {
	public static CancelPaymentCommand instance = new CancelPaymentCommand();
	private final OfflinePay op = OfflinePay.getOfflinePay();
	private OfflinePlayer paymentsFrom;
	private OfflinePlayer paymentsTo;
	boolean useTo;
	boolean useAmount;
	boolean useDelay;
	boolean useRepeats;
	private boolean other;

	/**
	 * Override the abstract method executed with the command
	 */
	@Override
	protected boolean doCommand() {
		other = command.getName().contains("other");
		if (other && args.length == 0) {
			return false;
		}
		if (!other && !(sender instanceof Player)) {// console doesn't have payments and thus can't cancel them
			sender.sendMessage(Messages.CONSOLE_CANCEL_PAYMENT.get());
			return true;
		}
		op.getServer().getScheduler().runTaskAsynchronously(op, new Runnable() {

			@Override
			public void run() {
				Map<String, String> map = getMap();
				try {
					setupVariables();
				} catch (PlayerNotFoundException e1) {
					sender.sendMessage(Messages.PLAYER_NOT_FOUND.get(map));
					return;
				}
				op.getServer().getScheduler().runTask(op, () -> {
					List<PeriodicPayment> matching = new ArrayList<PeriodicPayment>();
					for (PeriodicPayment pmnt : OfflinePay.getOfflinePay().getPayments()) {
						if (checkMatch(pmnt)) {
							matching.add(pmnt);
						}
					}
					int nr = -1;
					if (args.length > 4) {
						try {
							nr = Integer.parseInt(args[4]);
						} catch (NumberFormatException e) {
							// don't bother -> nr remains 0
						}
					}
					map.put("\\{matching\\}", String.join(", ",
							matching.stream().map(PeriodicPayment::toString).collect(joining(",\n"))));
					if (matching.isEmpty()) {
						sender.sendMessage(Messages.NO_MATCHING_PAYMENTS.get(map));
					} else if (matching.size() > 1 && nr == -1) {
						sender.sendMessage(Messages.MATCHING_MULTIPLE.get(map));
					} else { // size == 0 or nr specified
						if (nr > matching.size()) {
							map.put("\\{nr\\}", String.valueOf(matching.size()));
							sender.sendMessage(Messages.MATCHING_NR_ERROR.get(map));
							return;
						}
						PeriodicPayment pmnt = matching.get((nr == -1) ? 0 : nr - 1);
						map.put("\\{payment\\}", pmnt.toString());
						sender.sendMessage(Messages.CANCELLING_PAYMENT.get(map));
						OfflinePay.getOfflinePay().removePayment(pmnt);
					}
					return;
				});
			}

		});
		return true;
	}

	/**
	 * Setup variables for the command
	 * 
	 * @throws PlayerNotFoundException when no player found
	 */
	private void setupVariables() throws PlayerNotFoundException {
		paymentsFrom = getOfflinePlayer(other, other);
		if (paymentsFrom == null) {
			throw new PlayerNotFoundException();
		}
		if (other) { // remove first
			args = Arrays.copyOfRange(args, 1, args.length);
		}
		paymentsTo = getOfflinePlayer(true, true);
		if (args.length > 1) {
			parseAmount(args[1]);
		}
		if (args.length > 2) {
			setDelay(args[2]);
		}
		if (args.length > 3) {
			setRepeats(args[3]);
		}
		useTo = paymentsTo != null;
		useAmount = amount != -1;
		useDelay = delay != -1L;
		useRepeats = repeats != -2L;
	}

	/**
	 * Check if the current periodic payment matches the paramters given
	 * 
	 * @param pmnt the payment in question
	 * @return returns true if it matches, false otherwise
	 */
	private boolean checkMatch(PeriodicPayment pmnt) {
		if (pmnt.getFromUUID().equals(paymentsFrom.getUniqueId())) {
			if (useTo && !pmnt.getToUUID().equals(paymentsTo.getUniqueId())) {
				return false;
			}
			if (useAmount && pmnt.getAmount() != amount) {
				return false;
			}
			if (useDelay && pmnt.getDelay() != delay) {
				return false;
			}
			if (useRepeats && pmnt.getRepeats() != repeats) {
				return false;
			}
			return true;
		} else {
			return false;
		}

	}

	/**
	 * Make a map of paramters given for output
	 * 
	 * @return
	 */
	private Map<String, String> getMap() {
		String ns = "(not specified)";
		Map<String, String> map = new HashMap<String, String>();
		map.put("\\{from\\}", paymentsFrom.getName());
		map.put("\\{to\\}", useTo ? paymentsTo.getName() : ns);
		map.put("\\{amount\\}", useAmount ? (OfflinePay.getOfflinePay().getMoneySymbol() + amount) : ns);
		map.put("\\{delay\\}", useDelay ? Utils.formatDateDiff(delay * 1000L) : ns);
		map.put("\\{repeats\\}", useRepeats ? String.valueOf(repeats) : ns);
		return map;
	}

}
