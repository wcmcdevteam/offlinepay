package me.mart.offlinepay.commands;

import org.bukkit.ChatColor;

import me.mart.offlinepay.OfflinePay;

/**
 * Handles the reload command
 * 
 * @author mart
 *
 */
public class ReloadCommand extends OfflinePayCommandBase {
	public static ReloadCommand instance = new ReloadCommand();

	/**
	 * Overrides the abstract class
	 * Reloads the plugin config
	 */
	@Override
	protected boolean doCommand() {
		OfflinePay.getOfflinePay().reloadConfig();
		OfflinePay.getOfflinePay().loadConfiguration();
		OfflinePay.getOfflinePay().loadScheduledPayments();
		sender.sendMessage(ChatColor.RED + "Config reloaded!");
		return true;
	}

}
