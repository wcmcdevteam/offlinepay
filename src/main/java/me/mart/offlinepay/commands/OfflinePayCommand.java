package me.mart.offlinepay.commands;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import me.mart.offlinepay.OfflinePay;
import me.mart.offlinepay.events.OfflinePayEvent;
import me.mart.offlinepay.utils.Messages;

/**
 * Handles the /offlinepay <player> <amount> command
 * 
 * @author mart
 *
 */
public class OfflinePayCommand extends NonConsoleOfflinePayCommand {
	public static OfflinePayCommand instance = new OfflinePayCommand();
	private final OfflinePay op = OfflinePay.getOfflinePay();
	
	/**
	 * Does the command. sender, command and args are already protected members
	 * 
	 * return  false if not enough arguments, true otherwise
	 */
	public boolean doCommand() {
		// /offlinepay <offlineplayer> <amount>
		if (args.length < 2) {
			return false;
		}
		op.getServer().getScheduler().runTaskAsynchronously(op, 
				new Runnable() {

					@Override
					public void run() {
						OfflinePlayer other = getOfflinePlayer();
						if (other == null) { // problem + already notified
							return; 
						}
						String amountstring = args[1];
						parseAmount(amountstring);
						if (amount == -1) { // problem + already reported
							return;
						}
						Player payfrom = (Player) sender; // has to be Player as otherwise you get ConsoleCannotUseCommandException above
						if (!OfflinePay.getOfflinePay().econ.has(payfrom, amount)) {
							sender.sendMessage(Messages.CANNOT_AFFORD.get());
							return;
						}
						op.getServer().getScheduler().runTask(op, () -> {
						
							OfflinePayEvent event = new OfflinePayEvent(payfrom, other, amount);
							Bukkit.getPluginManager().callEvent(event);
							if (!event.isCancelled()) {
								OfflinePay.getOfflinePay().payFromTo(payfrom, other, amount);
							} else {
								if (OfflinePay.getOfflinePay().logPaymentsToConsole) {
									Map<String, String> map = new HashMap<String, String>();
									map.put("\\{from\\}", payfrom.getName());
									map.put("\\{to\\}", other.getName());
									map.put("\\{amount\\}", "" + amount);
									OfflinePay.getOfflinePay().getLogger().info(Messages.PAYMENT_CANCELLED.get(map));
								}
								sender.sendMessage(ChatColor.RED + "Payment cancelled!");
							}
						});
					} 
		
		});
		return true;
		
	}
	// tab completion defaults handled in OfflinePayCommandBase

}
