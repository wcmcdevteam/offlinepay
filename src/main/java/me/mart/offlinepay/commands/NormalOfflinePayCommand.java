package me.mart.offlinepay.commands;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import me.mart.offlinepay.OfflinePay;
import me.mart.offlinepay.exceptions.ConsoleCannotUseCommandException;
import me.mart.offlinepay.exceptions.PlayerNotFoundException;
import me.mart.offlinepay.utils.Messages;

/**
 * A command that console can also use - /mypayments and /oldpayments fall here as well as /cancelpayment and /paymentstome
 * 
 * @author mart
 *
 */
public abstract class NormalOfflinePayCommand extends OfflinePayCommandBase {

	/**
	 * Find the offline player being looked for. If one can't be found, respont appropriately and return null
	 * 
	 * @param canOther  whether or not a player other than the sender can be considered
	 * @param needOther whether or not a player other than the sender is necessarily needed
	 * @return          the offlineplayer if found, null otherwise
	 */
	protected OfflinePlayer getOfflinePlayer(boolean canOther, boolean needOther) {
		OfflinePlayer other = null;
		try {               						// ,consoleCan=true
			other = super.getOfflinePlayer(canOther, needOther, true); 
		} catch (ConsoleCannotUseCommandException e) {
			OfflinePay.LOGGER.severe("Got here and shouldn't be here!"); // should not get here...
			return null;
		} catch (PlayerNotFoundException e) { // no player found
			if (!(sender instanceof Player)) {
				sender.sendMessage(command.getUsage()); // get usage
			} else {
				Map<String, String> map = new HashMap<String, String>();
				map.put("\\{player\\}", args[0]);
				sender.sendMessage(Messages.PLAYER_NOT_FOUND.get(map));
			}
			return null;
		}
		if (needOther &&
				sender.getName().equalsIgnoreCase(other.getName())) {
			return null;
		}
		return other; // null if all else fails
	}

}
