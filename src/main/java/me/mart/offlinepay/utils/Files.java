package me.mart.offlinepay.utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import me.mart.offlinepay.OfflinePay;
import me.mart.offlinepay.PeriodicPayment;
import me.mart.offlinepay.Scheduler;

/**
 * Files used within the plugin
 * @author mart
 *
 */
public enum Files {
	MESSAGES("messages","messages.yml"),
	SCHEDULED_PAYMENTS("scheduled payments", "scheduled_payments.yml"),
	OLD_SCHEDULED_PAYMENTS("old scheduled payments", "old_scheduled_payments.yml")
	;
	
	private final String filename;
	private final String filepath;
	private final FileConfiguration file;
	private final File filefile;
	/**
	 * @param name name of the file
	 * @param path path of the file within datafolder
	 */
	private Files(String name, String path) {
		filename = name;
		filepath = path;
		filefile = new File(OfflinePay.getOfflinePay().getDataFolder(), filepath);
		file = YamlConfiguration.loadConfiguration(filefile);
	}
	
	/**
	 * @return  file
	 */
	public FileConfiguration getFile() {
		return file;
	}
	
	/**
	 * @return  file name
	 */
	public String getFilename() {
		return filename;
	}
	
	/**
	 * @return  file path in dataFolder
	 */
	public String getFilepath() {
		return filepath;
	}
	
	/**
	 * @return  file the config is saved at
	 */
	public File getFileFile() {
		return filefile;
	}
	
	/**
	 *   save the config
	 */
	public void save() {
		try {
			file.save(filefile);
		} catch (IOException e) {
			OfflinePay.LOGGER.warning("Unable to save file:" + filename);
			e.printStackTrace();
		}
	}
	
	
	/**
	 * Add a PeriodicPayment to the active payments file
	 * 
	 * @param pmnt  Payment to add
	 */
	public static void addPeriodicPayment(PeriodicPayment pmnt) {
		addPeriodicPayment(pmnt, false);
	}
	
	/**
	 * Add a PeriodicPayment to the old payments file
	 * 
	 * @param pmnt Periodic payment
	 */
	public static void addPeriodicPayment(PeriodicPayment pmnt, boolean old) {
		Files filecontainer = Files.SCHEDULED_PAYMENTS;
		if (old) {
			filecontainer = Files.OLD_SCHEDULED_PAYMENTS;
		}
		FileConfiguration file = filecontainer.getFile();
		String basepath = getPaymentBasePath(pmnt);
		// just for readability
		String fromName = Bukkit.getOfflinePlayer(pmnt.getFromUUID()).getName();
		String toName = Bukkit.getOfflinePlayer(pmnt.getToUUID()).getName();
		OfflinePay.LOGGER.info("Created new Periodic payment in config " + filecontainer.getFilepath() + ":");
		OfflinePay.LOGGER.info(basepath);
		OfflinePay.LOGGER.info("From " + fromName + " to " + toName);
		file.set(basepath + "from_name", fromName);
		file.set(basepath + "to_name", toName);
		// needed
		file.set(basepath + "fromUUID", pmnt.getFromUUID().toString());
		file.set(basepath + "toUUID", pmnt.getToUUID().toString());
		file.set(basepath + "amount", pmnt.getAmount());
		file.set(basepath + "delay", pmnt.getDelay());
		file.set(basepath + "repeats", pmnt.getRepeats());
		file.set(basepath + "repeats-left", pmnt.getRepeatsLeft());
		file.set(basepath + "repeated", pmnt.getRepeated());
		file.set(basepath + "failed", pmnt.getFailed());
		file.set(basepath + "lastpaid", pmnt.getLastPaid());
		file.set(basepath + "nextpaymenttime", pmnt.getNextPayTime());
		file.set(basepath + "starttime", pmnt.getStartTime());
		filecontainer.save();
	}
	
	/**
	 * Get the path in config wich the PeriodicPayment is/will be under in the file
	 * 
	 * @param pmnt  Payment in question
	 * @return      path in config
	 */
	public static String getPaymentBasePath(PeriodicPayment pmnt) {
		String basepath = pmnt.getFromUUID().toString() + "_" + pmnt.getToUUID().toString();
		basepath += "_" + pmnt.getStartTime() + ".";
		return basepath;
	}
	
	/**
	 * Update periodicy Paymetns file with an instance (i.e each time a payment is attempted)
	 * 
	 * @param pmnt  periodic payment in question
	 */
	public static void updatePeriodicPayment(PeriodicPayment pmnt) {
		Files filecontainer = Files.SCHEDULED_PAYMENTS;
		FileConfiguration file = filecontainer.getFile();
		String basepath = getPaymentBasePath(pmnt);
		if (file.isConfigurationSection(basepath)) {
			// update stuff
			file.set(basepath + "repeats-left", pmnt.getRepeatsLeft());
			file.set(basepath + "repeated", pmnt.getRepeated());
			file.set(basepath + "failed", pmnt.getFailed());
			file.set(basepath + "lastpaid", pmnt.getLastPaid());
			file.set(basepath + "nextpaymenttime", pmnt.getNextPayTime());
		} else {
			OfflinePay.LOGGER.warning("Had to create new entry while updating Periodic payment!");
			addPeriodicPayment(pmnt);
		}
		filecontainer.save();
	}
	
	/**
	 * @return LOAD list of active PeriodicPayments from file
	 */
	public static List<PeriodicPayment> loadPeriodicPayments(){
		return loadPeriodicPayments(false);
	}
	
	/**
	 * LOAD list of active OR OLD periodici payments from file
	 * 
	 * @param old  whether or not to load old payments
	 * @return     list of PeriodicPayments loaded
	 */
	public static List<PeriodicPayment> loadPeriodicPayments(boolean old){
		Files filecontainer = Files.SCHEDULED_PAYMENTS;
		if (old) {
			filecontainer = Files.OLD_SCHEDULED_PAYMENTS;
		}
		FileConfiguration file = filecontainer.getFile();
		List<PeriodicPayment> pmnts = new ArrayList<PeriodicPayment>();
		for (String path : file.getConfigurationSection("").getKeys(false)) {
			if (file.getConfigurationSection(path).getKeys(true).size() > 0) {
				// ignore ID1_ID2: {}
				OfflinePlayer payfrom = Bukkit.getOfflinePlayer(UUID.fromString(file.getString(path + ".fromUUID")));
				OfflinePlayer payto = Bukkit.getOfflinePlayer(UUID.fromString(file.getString(path + ".toUUID")));
				double amount = file.getDouble(path + ".amount");
				long delay = file.getLong(path + ".delay");
				long repeats = file.getLong(path + ".repeats");
				long repeatsLeft = file.getLong(path + ".repeats-left");
				long repeated = file.getLong(path + ".repeated");
				long failed = file.getLong(path + ".failed");
				long lastpaid = file.getLong(path + ".lastpaid");
				long nextpaymenttime = file.getLong(path + ".nextpaymenttime");
				long starttime = file.getLong(path + ".starttime");
				PeriodicPayment pmnt = new PeriodicPayment(payfrom, payto, amount, delay, repeats, 
						repeatsLeft, repeated, failed, lastpaid, nextpaymenttime, starttime);
				if (pmnt.moreToPay() || old) {
					pmnts.add(pmnt);
					if (!old) {
						Scheduler.schedulePeriodicPayment(pmnt);
					}
				} else {
					removePeriodicPayment(pmnt);
				}
			}
		}
		return pmnts;
	}
	
	/**
	 * Save the active PeriodicPayments file
	 */
	public static void savePeriodicPayments() {
		savePeriodicPayments(false);
	}
	
	/**
	 * Save the active OR OLD PeriodicPayments file
	 * 
	 * @param old  whether or not to use the old payments' file
	 */
	public static void savePeriodicPayments(boolean old) {
		Files filecontainer = Files.SCHEDULED_PAYMENTS;
		if (old) {
			filecontainer = Files.OLD_SCHEDULED_PAYMENTS;
		}
		filecontainer.save();
	}
	
	/**
	 * Remove a PeriodicPayment from the active file and move it to the OLD one
	 * 
	 * @param pmnt  payment in question
	 */
	public static void removePeriodicPayment(PeriodicPayment pmnt) {
		Files filecontainer = Files.SCHEDULED_PAYMENTS;
		FileConfiguration file = filecontainer.getFile();
		String basestring = getPaymentBasePath(pmnt);
		file.set(basestring.substring(0,  basestring.length() - 1), null); //remove "."
		addPeriodicPayment(pmnt, true); // add to old
		savePeriodicPayments();
		savePeriodicPayments(true); // save old
	}
	

}
