package me.mart.offlinepay.utils;

import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.ChatColor;

/**
 * Messages used within the plugin
 * @author mart
 *
 */
public enum Messages {
	CONSOLE_OFFLINEPAY("console.offlinepay", "&cConsole user should use Essentials' /eco command"),
	AMOUNT_MUST_BE_POSITIVE("payment.amount-must-be-positive", "&cAmount must be positive!"),
	MINIMUM_PAYMENT("payment.minimum", "&cNeed to send at least &8{amount}"),
	CANNOT_AFFORD("payment.cannot-afford", "&cCannot afford this!"),
	PROVIDE_AMOUNT("payment.provide_amount", "&cYou need to provide a number to pay"),
	PLAYER_NOT_FOUND("player.not-found", "&cPlayer not found: &8{player}"),
	CANNOT_PAY_SELF("player.cannot-pay-self", "&cYou cannot pay yourself!"),
	SENT_MONEY("payment.sent-money", "&aYou sent &7{amount}&a to &8{player}"),
	GOT_MONEY("payment.got-money", "&aYou got &7{amount}&a from &8{player}"),
	PROVIDE_TIME("periodicpay.provide-time", "&cYou need to provide a proper delay between the payments. For example &71mo&c or &71w"),
	PROVIDE_REPEATS("periodicpay.provide-repeats", "&cYou need to provide a number of repeats (or \"inf\")"),
	PAYMENT_CANCELLED("payment.cancelled", "&cPayment from &3{from}&c to &3{to}&c in the amount &8{amount}&c was cancelled!"),
	START_PERIODICPAY("periodicpay.start", "&3Sending &8{amount}&3 to &1{to}&3 every &8{delay}&3 a total of &8{repeats}&3 times"),
	LOG_PAYMENT_TO_CONSOLE("payment.log-to-console", "&1{from}&7 paid &3{amount}&7 to &1{to}"),
	FAILED_PAY_ME("periodicpay.failed-to-pay-me", "&8{from} &c failed to pay &3{amount}"),
	FAILED_PAY("periodicpay.failed-to-pay", "&cI failed to pay &8{to} &3{amount}"),
	FAILED_PAY_FROM_TO("periodicpay.failed-pay-from-to", "&1{from} &7failed to pay &3{amount}&7 to &1{to}"),
	MINIMUM_DELAY("periodicpay.min-delay", "&cYou need to have a delay of at least &3{mindelay}&c!"),
	MUST_BE_PLAYER("console.must-be-player", "&cA player must issue this command!"),
	PAYMENT_DESCRIPTION("periodicpay.description", "&6A periodic payment of &1{amount} from &8{from}&6 to &8{to}&6 every " +
			"&1{delay}&6. &9{done}&6/&9{repeats}&6 payments made (&4{failed}&6 failed). " +
			"Next payment &1{nextpay}&6from now, last payment " +
			"&1{lastpaid}&6ago."),
	PAYMENT_DESCRIPTION_OLD("periodicpay.description_old", "&6A periodic payment of &1{amount} from &8{from}&6 to &8{to}&6 every " +
			"&1{delay}&6. &9{done}&6/&9{repeats}&6 payments made (&4{failed}&6 failed). " +
			"Next payment &1{nextpay}&6ago, last payment " +
			"&1{lastpaid}&6ago."),
	START_PERIODICPAY_LIST("periodicpay.start-list", "&6{active} periodic payments {tofrom} &8{player}&6 - page &8{page}"),
	NO_ACTIVE_PAYMENTS("periodicpay.no-active-payments", "&6No active payments found onf this page"),
	NO_OLD_PAYMENTS("periodicpay.no-old-payments", "&6No old payments found on this page"),
	CONSOLE_CANCEL_PAYMENT("console.cancel-payment", "&cUse &1/cancelotherpayment &cto cancel players's payments"),
	NO_MATCHING_PAYMENTS("periodicpay.no-matching", "&cNo matching payments found where &8{from}&c pays &8{to} &3{amount}&c every &3{delay}&ca total of &3{repeats}&c times. You may need to add more parameters to the search."),
	MATCHING_MULTIPLE("periodicpay.multiple-matching", "&6Multiple matching payments found:{matching}"),
	CANCELLING_PAYMENT("periodicpay.cancelling-payment", "&6Cancelling payment: {payment}"),
	MATCHING_NR_ERROR("periodicpay.cancel-matching-nr-error", "&cNeed to proivde a number that is smaller than the number of available choises({nr})!"),
	;
	
	private final String path;
	private String message;
	
	/**
	 * @param path    path of message within file
	 * @param message default message
	 */
	private Messages(String path, String message) {
		this.path = path;
		this.message = message;
	}
	
	/**
	 * @return coloured message
	 */
	public String get() {
		if (Files.MESSAGES.getFile().contains(path)) { 
			// message from file
			return c(Files.MESSAGES.getFile().getString(path));
		} else {
			// default message
			return c(message);
		}
	}
	
	/**
	 * Get (coloured) message where a map is used to swap out {} tagged stuff 
	 * 
	 * @param map  map used to replace
	 * @return     the coloured message
	 */
	public String get(Map<String, String> map) {
		String msg = message;
		if (Files.MESSAGES.getFile().contains(path)) {
			msg = Files.MESSAGES.getFile().getString(path);
		}
		for (Entry<String, String> entry : map.entrySet()) {
			msg = msg.replaceAll(entry.getKey(), entry.getValue());
		}
		return c(msg);
	}
	
	/**
	 * Colours message
	 * 
	 * @param msg message to color
	 * @return    coloured message
	 */
	public String c(String msg) {
		if (msg == null) {
			return "";
		}
		return ChatColor.translateAlternateColorCodes('&', msg);
	}

}
