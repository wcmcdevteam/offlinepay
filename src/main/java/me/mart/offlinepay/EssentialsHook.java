package me.mart.offlinepay;

import java.util.UUID;

import com.earth2me.essentials.IEssentials;

public class EssentialsHook {
    private final OfflinePay plugin;
    private final IEssentials ess;
    
    public EssentialsHook(OfflinePay plugin) {
        this.plugin = plugin;
		ess = (IEssentials) this.plugin.getServer().getPluginManager().getPlugin("Essentials");
    }

    public void mailTo(UUID id, String msg) {
        ess.getUser(id).addMail(msg);
    }
    
}