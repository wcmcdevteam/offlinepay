package me.mart.offlinepay;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Logger;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import me.mart.offlinepay.commands.CancelPaymentCommand;
import me.mart.offlinepay.commands.OfflinePayCommand;
import me.mart.offlinepay.commands.PaymentsCommand;
import me.mart.offlinepay.commands.PeriodicPayCommand;
import me.mart.offlinepay.commands.ReloadCommand;
import me.mart.offlinepay.exceptions.PlayerNotFoundException;
import me.mart.offlinepay.listeners.OfflinePaymentListener;
import me.mart.offlinepay.utils.Files;
import me.mart.offlinepay.utils.Messages;
import net.milkbowl.vault.economy.Economy;

/**
 * @author mart
 * Plugin to allow people to pay offline players
 * Also allows paying of "rent" periodically
 *
 */
public class OfflinePay extends JavaPlugin {
	public Economy econ = null;
	private EssentialsHook essHook;
	private boolean sendMailToOffline = false;
	public boolean logPaymentsToConsole = false;
	public boolean hasEssentials = false;
	public boolean notifyFailures = true;
	private double minPay;
	private long mindelay;
	private String moneySymbol;
	private String ms;
	private long tryAgainTime;
	private boolean enableTabComplete = true;
	List<PeriodicPayment> payments = new ArrayList<PeriodicPayment>();
	
	public final static Logger LOGGER = Logger.getLogger("OfflinePay");
	
	/* Method run on plugin startup
	 * (non-Javadoc)
	 * @see org.bukkit.plugin.java.JavaPlugin#onEnable()
	 */
	public void onEnable() {
        getCommand("offlinepay").setExecutor(OfflinePayCommand.instance);
        getCommand("offlinepay").setTabCompleter(OfflinePayCommand.instance);
        getCommand("periodicpay").setExecutor(PeriodicPayCommand.instance);
        getCommand("periodicpay").setTabCompleter(PeriodicPayCommand.instance);
        getCommand("offlinepayreload").setExecutor(ReloadCommand.instance);
        getCommand("offlinepayreload").setTabCompleter(ReloadCommand.instance);
        getCommand("mypayments").setExecutor(PaymentsCommand.instance);
        getCommand("mypayments").setTabCompleter(PaymentsCommand.instance);
        getCommand("oldpayments").setExecutor(PaymentsCommand.instance);
        getCommand("oldpayments").setTabCompleter(PaymentsCommand.instance);
        getCommand("paymentstome").setExecutor(PaymentsCommand.instance);
        getCommand("paymentstome").setTabCompleter(PaymentsCommand.instance);
        getCommand("oldpaymentstome").setExecutor(PaymentsCommand.instance);
        getCommand("oldpaymentstome").setTabCompleter(PaymentsCommand.instance);
        getCommand("cancelpayment").setExecutor(CancelPaymentCommand.instance);
        getCommand("cancelpayment").setTabCompleter(CancelPaymentCommand.instance);
        getCommand("cancelotherpayment").setExecutor(CancelPaymentCommand.instance);
        getCommand("cancelotherpayment").setTabCompleter(CancelPaymentCommand.instance);
        
		getServer().getPluginManager().registerEvents(new OfflinePaymentListener(), this);
		if (!setupVault()) {
			LOGGER.severe("Could not load Vault. Closing plugin");
			this.getServer().getPluginManager().disablePlugin(this);
		}
		hasEssentials = setupEssentials();
		if (!hasEssentials) {
			LOGGER.warning("Essentials not found. Some functionality not retrained!");
		}
		loadConfiguration();
		loadScheduledPayments();
		LOGGER.info("Has essentials:" + hasEssentials + ";notify on failure:" + notifyFailures + ";SendMailToOffline:" + sendMailToOffline);
	}
	
	/**
	 * Load configuration
	 */
	public void loadConfiguration() {
		getConfig().options().copyDefaults(true);
		minPay = getConfig().getDouble("money.minpay");
		moneySymbol = getConfig().getString("money.symbol");
		tryAgainTime = getConfig().getLong("periodicpay.try-again-time");
		ms = moneySymbol;
		if (moneySymbol.equals("$")) {
			ms = "\\$";
		}
		sendMailToOffline = getConfig().getBoolean("send-mail-to-offline");
		logPaymentsToConsole = getConfig().getBoolean("log-payments-to-console");
		notifyFailures = getConfig().getBoolean("periodicpay.send-mail-on-failure");
		mindelay = getConfig().getLong("periodicpay.min-delay");
		enableTabComplete = getConfig().getBoolean("enable-tab-complete");
		saveConfig();
		Files.MESSAGES.getFile().options().copyDefaults(true);
		saveResource(Files.MESSAGES.getFilepath(), false);
	}
	
	/**
	 * Load active periodic payments from disk
	 */
	public void loadScheduledPayments() {
		payments = Files.loadPeriodicPayments();
	}
	
	
	/**
	 * @return whether or not vault was successfully implemented
	 */
	public boolean setupVault() {
		if (getServer().getPluginManager().getPlugin("Vault") == null) {
			return false;
		}
		RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
           return false;
        } else {
           econ = (Economy)rsp.getProvider();
           return econ != null;
        }
	}
	
	/**Setup Essentials if available
	 * 
	 * @return
	 */
	public boolean setupEssentials() {
		if (getServer().getPluginManager().getPlugin("Essentials") == null) {
			return false;
		}
		essHook = new EssentialsHook(this);
		return true;
	}
	
	/**
	 * @return this plugin
	 */
	public static OfflinePay getOfflinePay() {
		return (OfflinePay) Bukkit.getPluginManager().getPlugin("OfflinePay");
	}
	
	/**Deals with payment from one player to another - both /offlinepay and /periodicpay
	 * OfflinePayEvent uses this method as well
	 * 
	 * @param payfrom user that pays
	 * @param payto   user that gets paid
	 * @param amount  amount paid
	 */
	public void payFromTo(OfflinePlayer payfrom, OfflinePlayer payto, double amount) {
		// checks that payer can afford must be done beforehands
		econ.withdrawPlayer(payfrom, amount);
		econ.depositPlayer(payto, amount);
		// handle messages
		Map<String, String> map = new HashMap<String, String>();
		map.put("\\{player\\}", payto.getName());
		map.put("\\{amount\\}", String.valueOf(ms + amount));
		if (payfrom.isOnline()) {
			Bukkit.getPlayer(payfrom.getUniqueId()).sendMessage(Messages.SENT_MONEY.get(map));
		} else {
			if (hasEssentials && sendMailToOffline) {
				mailPlayer(payfrom, Messages.SENT_MONEY.get(map));
			}
		}
		map.put("\\{player\\}", payfrom.getName());
		if (payto.isOnline()) {
			Bukkit.getPlayer(payto.getUniqueId()).sendMessage(Messages.GOT_MONEY.get(map));
		} else {
			if (hasEssentials && sendMailToOffline) {
				mailPlayer(payto, Messages.GOT_MONEY.get(map));
			}
		}
		if (logPaymentsToConsole) {
			map.put("\\{from\\}", payfrom.getName());
			map.put("\\{to\\}", payto.getName());
			LOGGER.info(Messages.LOG_PAYMENT_TO_CONSOLE.get(map));
		}
	}
	
	/**Mails OfflinePlayer (if Essentials available)
	 * 
	 * @param op  offline player to mail
	 * @param msg message to sned
	 */
	public void mailPlayer(OfflinePlayer op, String msg) {
		mailPlayer(op.getUniqueId(), msg);
	}
	
	/**Mails player with UUID (if Essentials available)
	 * 
	 * @param id
	 * @param msg
	 */
	public void mailPlayer(UUID id, String msg) {
		if (hasEssentials) {
			essHook.mailTo(id, msg);
		}
	}
	
	/**Deals with paymenf from a Player to an OfflinePlayer
	 * 
	 * @param payfrom user that pays
	 * @param payto   user that gets paid
	 * @param amount  amount paid
	 */
	public void payFromTo(Player payfrom, OfflinePlayer payto, double amount) {
		payFromTo((OfflinePlayer) payfrom, payto, amount);
	}
	
	/**
	 * Gets the offline player from a string argument
	 * 
	 * @param arg  the name of the player being searched for
	 * @return     the player, if found
	 * @throws PlayerNotFoundException  if no player by that name has joined, throw the exception
	 */
	public OfflinePlayer getOfflinePlayerFromArg(String arg) throws PlayerNotFoundException {
		@SuppressWarnings("deprecation")
		OfflinePlayer op = Bukkit.getOfflinePlayer(arg);
		if (op == null || !op.hasPlayedBefore()) {
			throw new PlayerNotFoundException();
		}
		return op;
	}
	
	/* (non-Javadoc)
	 * @see org.bukkit.plugin.java.JavaPlugin#onCommand(org.bukkit.command.CommandSender, org.bukkit.command.Command, java.lang.String, java.lang.String[])
	 */
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		// commands handled in their separate classes, retsitrations in onEnable
		return false;
	}
	
	/**Returns Essentials plugin
	 * 
	 * @return essentials plugin 
	 */
	public EssentialsHook getEssentials() {
		return essHook;
	}
	
	/**
	 * @return  the minimum payment allowed
	 */
	public double getMinPay() {
		return minPay;
	}
	
	/**
	 * @return  the money symbol used
	 */
	public String getMoneySymbol() {
		return ms;
	}
	
	/**
	 * @return  the minimum delay allowed
	 */
	public long getMinDelay() {
		return mindelay;
	}
	
	/**
	 * @return  whether or not tabcompletion is enabled
	 */
	public boolean getEnableTabComplete() {
		return enableTabComplete;
	}
	
	/**
	 * Checks whether or not a payment is in active payments (in case it was cancelled)
	 * 
	 * @param pmnt  Payment to check
	 * @return      whether or not payment in active payments
	 */
	public boolean paymentExists(PeriodicPayment pmnt) {
		return payments.contains(pmnt);
	}

	/**
	 * Add a new active periodic payment
	 * 
	 * @param pmnt  the payment being added
	 */
	public void addPayment(PeriodicPayment pmnt) {
		payments.add(pmnt);
	}

	/**
	 * Remove a (perivously) active periodic payment
	 * 
	 * @param pmnt  payment to be removed
	 */
	public void removePayment(PeriodicPayment pmnt) {
		Files.removePeriodicPayment(pmnt);
		payments.remove(pmnt);
	}

	/**
	 * @return  active periodic payments
	 */
	public List<PeriodicPayment> getPayments() {
		return payments;
	}
	
	/**
	 * @return  time (in seconds) after which a failed payment is retried by default.
	 */
	public long getTryAgainTime() {
		return tryAgainTime;
	}
}
