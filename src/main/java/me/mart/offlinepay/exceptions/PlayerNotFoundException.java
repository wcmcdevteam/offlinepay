package me.mart.offlinepay.exceptions;

/**
 * Exception for when the offline player was not foudn
 * 
 * @author mart
 *
 */
public class PlayerNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -715452827628025245L;
	
	/**
	 * Constructor
	 */
	public PlayerNotFoundException() {
		super("Player not found!");
	}

}
