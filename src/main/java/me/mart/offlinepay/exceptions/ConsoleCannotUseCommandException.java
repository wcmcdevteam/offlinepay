package me.mart.offlinepay.exceptions;

/**
 * Exception for commands the console cannot use
 * 
 * @author mart
 *
 */
public class ConsoleCannotUseCommandException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7569500956235686201L;

	/**
	 * Constructor
	 */
	public ConsoleCannotUseCommandException() {
		super("Console cannot use this command!");
	}
}
