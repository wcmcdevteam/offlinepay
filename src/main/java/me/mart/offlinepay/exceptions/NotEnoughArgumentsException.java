package me.mart.offlinepay.exceptions;

/**
 * Exception for when there are not enough arguments
 * 
 * @author mart
 *
 */
public class NotEnoughArgumentsException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2044711060587895548L;
	
	/**
	 * Constructor
	 */
	public NotEnoughArgumentsException() {
		super("Not enough arguments for command!");
	}
}
