package me.mart.offlinepay;

/**
 * Handles scheduling of periodic payments
 * 
 * @author mart
 *
 */
public class Scheduler {
	/**Schedules a payment at next payment time of the PeriodicPayment
	 * 
	 * @param pmnt the periodic payment in question
	 */
	public static void schedulePeriodicPayment(PeriodicPayment pmnt) {
		schedulePeriodicPayment(pmnt, (pmnt.getNextPayTime() - System.currentTimeMillis())/1000L);
	}
	
	/**Schedules a payment of a PeriodicPayment at a certain delay
	 * 
	 * @param pmnt  the periodic payment in question
	 * @param delay the delay in seconds
	 */
	public static void schedulePeriodicPayment(PeriodicPayment pmnt, long delay) {
		// delay in seconds !
		OfflinePay.getOfflinePay().getServer().getScheduler().runTaskLater(OfflinePay.getOfflinePay(), new Runnable() {

			@Override
			public void run() {
				if (OfflinePay.getOfflinePay().paymentExists(pmnt) && pmnt.moreToPay()) {
					boolean success = pmnt.tryToPay();
					if (!success) {// reschedule
						long delayleft = pmnt.getNextPayTime() - System.currentTimeMillis();
						if (OfflinePay.getOfflinePay().getTryAgainTime() * 1000L > delayleft) { // if less time left than tryAgainTime
							if (delayleft < OfflinePay.getOfflinePay().getMinDelay() * 1000L) { // avoid going going too fast
								delayleft = 2 * OfflinePay.getOfflinePay().getMinDelay() * 1000L; 
							}// half the time left until next payment
							schedulePeriodicPayment(pmnt, delayleft/2L/1000L);
						} else {
							schedulePeriodicPayment(pmnt, OfflinePay.getOfflinePay().getTryAgainTime());
						}
					} else { // on success
						if (pmnt.moreToPay()) { // schedule next payment if necessary
							schedulePeriodicPayment(pmnt);
						} else { // if no more payments needed, remove payment
							OfflinePay.getOfflinePay().removePayment(pmnt);
						}
					}
				}
				
			}
			
		}, delay * 20L);
	}
}
