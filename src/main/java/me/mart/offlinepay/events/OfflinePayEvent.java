package me.mart.offlinepay.events;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * Event triggered when a payment is made (either with /offlinepay or /periodicpay)
 * 
 * @author mart
 *
 */
public class OfflinePayEvent extends Event implements Cancellable{
    private static final HandlerList handlers = new HandlerList();
	private UUID fromUUID;
	private UUID toUUID;
	private double amount;
	private boolean cancelled = false;
	
	/**
	 * Constructor from UUID
	 * 
	 * @param from    the player who pays
	 * @param to      the player who is paid
	 * @param amount  the amound being paid
	 */
	public OfflinePayEvent(UUID from, UUID to, double amount) {
		fromUUID = from;
		toUUID = to;
		this.amount = amount;
	}
	
	/**
	 * Constructor from players
	 * 
	 * @param fromPlayer
	 * @param toPlayer
	 * @param amount
	 */
	public OfflinePayEvent(Player fromPlayer, OfflinePlayer toPlayer, double amount) {
		this(fromPlayer.getUniqueId(), toPlayer.getUniqueId(), amount);
	}
	
	/**
	 * @return amount  being paid
	 */
	public double getAmount() {
		return amount;
	}
	
	/**
	 * Set a new amount
	 * 
	 * @param newValue  new value for amount
	 */
	public void setAmount(double newValue) {
		amount = newValue;
	}

	/**
	 * @return  player who pays
	 */
	public OfflinePlayer getFromPlayer() {
		return Bukkit.getOfflinePlayer(fromUUID);
	}
	
	/**
	 * @return  player who is paid
	 */
	public OfflinePlayer getToPlayer() {
		return Bukkit.getOfflinePlayer(toUUID);
	}

	/**
	 * @return  UUID of player who pays
	 */
	public UUID getFromUUID() {
		return fromUUID;
	}
	
	/**
	 * @return  UUID of player who is paid
	 */
	public UUID getToUUID() {
		return toUUID;
	}	
	
	/* (non-Javadoc)
	 * @see org.bukkit.event.Event#getHandlers()
	 */
	@Override
	public HandlerList getHandlers() {
		return handlers;
	}
	
	/**
	 * @return list of handlers
	 */
	public static HandlerList getHandlerList() {
		return handlers;
	}

	/** 
	 * return whether ot not the event has been cancelled
	 */
	@Override
	public boolean isCancelled() {
		return cancelled;
	}

	/**
	 * Set whether or not the even is cancelled
	 * 
	 * @param cancelled  new value for cancelled
	 */
	@Override
	public void setCancelled(boolean cancelled) {
		this.cancelled = cancelled;
	}

}
