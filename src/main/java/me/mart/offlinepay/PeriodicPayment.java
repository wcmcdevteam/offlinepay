package me.mart.offlinepay;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import me.mart.offlinepay.events.OfflinePayEvent;
import me.mart.offlinepay.utils.Files;
import me.mart.offlinepay.utils.Messages;
import me.mart.offlinepay.utils.Utils;

/**
 * Handles periodic payments
 * 
 * @author mart
 *
 */
public class PeriodicPayment {
	private final UUID fromUUID;
	private final UUID toUUID;
	private final double amount;
	private final long delay;
	private final long repeats;
	private long repeatsLeft;
	private long repeated = 0L;
	private long failed = 0L;
	private long lastpaid = System.currentTimeMillis();
	private long nextpayment;
	private final long starttime;
	
	/**
	 * Create a new PeriodicPayment
	 * 
	 * @param payfrom player that pays
	 * @param payto   player that is paid
	 * @param amount  amount used
	 * @param delay   delay between payments (in seconds)
	 * @param repeats number of repeats (-1 if infinite)
	 */
	public PeriodicPayment(Player payfrom, OfflinePlayer payto, double amount, long delay, long repeats) {
		fromUUID = payfrom.getUniqueId();
		toUUID = payto.getUniqueId();
		this.amount = amount;
		this.delay = delay;
		this.repeats = repeats;
		repeatsLeft = repeats;
		nextpayment = System.currentTimeMillis() + this.delay * 1000L;
		starttime = System.currentTimeMillis();
	}
	
	/**
	 * Recreate an exisint PeriodicPayment from files.
	 * 
	 * @param payfrom      player who pays
	 * @param payto        player
	 * @param amount       amount to be paid
	 * @param delay        delay between payments (in seconds)
	 * @param repeats      number of repeats to be done (-1 if infinite)
	 * @param repeatsLeft  how many repeats left
	 * @param repeated     how many times already repeated
	 * @param failed       how many times failed until now
	 * @param lastpaid     when last paid (in milliseconds)
	 * @param nextpayment  when next payment is due (in milliseconds)
	 * @param starttime    when payment was initially start (in milliseconds)
	 */
	public PeriodicPayment(OfflinePlayer payfrom, OfflinePlayer payto, double amount, long delay, long repeats, 
								long repeatsLeft, long repeated, long failed, long lastpaid, long nextpayment, long starttime) {
		fromUUID = payfrom.getUniqueId();
		toUUID = payto.getUniqueId();
		this.amount = amount;
		this.delay = delay;
		this.repeats = repeats;
		this.repeatsLeft = repeatsLeft;
		this.repeated = repeated;
		this.failed = failed;
		this.lastpaid = lastpaid;
		this.nextpayment = nextpayment;
		this.starttime = starttime;
	}
	
	/**
	 * @param event  the triggered event (hasn't been cancelled here)
	 */
	private void dealWithPayment(OfflinePayEvent event) {
		// using event's from, to and amount in case someone changed them in an event handler...
		// messaging is dealt with within payFromTo
		OfflinePay.getOfflinePay().payFromTo(event.getFromPlayer(), event.getToPlayer(), event.getAmount());
		repeated ++;
		if (repeats != -1L) {
			repeatsLeft --;
		}
		lastpaid = System.currentTimeMillis();
		nextpayment += delay * 1000L;
	}
	
	/**
	 * @param event  the triggered event (has been cancelled here)
	 * @param map    the map used for replacing in message
	 */
	private void dealWithFailure(OfflinePayEvent event, Map<String, String> map) {
		if (OfflinePay.getOfflinePay().hasEssentials && OfflinePay.getOfflinePay().notifyFailures) {
			if (event.getFromPlayer().isOnline()) {
				OfflinePay.getOfflinePay().getServer().getPlayer(event.getFromUUID()).sendMessage(Messages.FAILED_PAY.get(map));
			} else {
				OfflinePay.getOfflinePay().mailPlayer(event.getFromUUID(), Messages.FAILED_PAY.get(map));
			}
			if (event.getToPlayer().isOnline()) {
				OfflinePay.getOfflinePay().getServer().getPlayer(event.getToUUID()).sendMessage(Messages.FAILED_PAY_ME.get(map));
			} else {
				OfflinePay.getOfflinePay().mailPlayer(event.getToUUID(), Messages.FAILED_PAY_ME.get(map));
			}
		}
		if (OfflinePay.getOfflinePay().logPaymentsToConsole) {
			OfflinePay.LOGGER.info(Messages.FAILED_PAY_FROM_TO.get(map));
		}
		failed ++;
	}
	
	/**
	 * Attempt payment
	 * 
	 * @return  whether or not the payment went through
	 */
	public boolean tryToPay() {
		OfflinePayEvent event = new OfflinePayEvent(fromUUID, toUUID, amount);
		Map<String, String> map = new HashMap<String, String>();
		map.put("\\{from\\}", event.getFromPlayer().getName());
		map.put("\\{to\\}", event.getToPlayer().getName());
		map.put("\\{amount\\}", String.valueOf(event.getAmount()));
		Bukkit.getPluginManager().callEvent(event);
		if (!event.isCancelled()) {
			dealWithPayment(event);
		} else {
			dealWithFailure(event, map);
		}
		Files.updatePeriodicPayment(this); // update file with this payment so to not lose data
		return !event.isCancelled();
	}
	/**
	 * Get the map for replacing in messages from the PeriodicPayment instance
	 * 
	 * @param pmnt  periodic payment the info comes from
	 * @return      map to use to replace in messages
	 */
	public Map<String, String> getMap(){
		Map<String, String> map = new HashMap<String, String>();
		OfflinePlayer op = Bukkit.getOfflinePlayer(fromUUID);
		if (op == null || !op.hasPlayedBefore()) {
			OfflinePay.LOGGER.severe(ChatColor.RED + "Could not load user from UUID:" + fromUUID.toString());
		}
		map.put("\\{from\\}", op.getName());
		map.put("\\{player\\}", map.get("\\{from\\}"));
		OfflinePlayer other = Bukkit.getOfflinePlayer(toUUID);
		if (other == null || !other.hasPlayedBefore()) {
			OfflinePay.LOGGER.severe(ChatColor.RED + "Could not load user from UUID:" + fromUUID.toString());
		}
		map.put("\\{to\\}", other.getName());
		map.put("\\{amount\\}", String.valueOf(amount));
		map.put("\\{delay\\}", Utils.formatDateFromDiff(delay*1000L));
		map.put("\\{repeats\\}", String.valueOf(repeats));
		map.put("\\{left\\}", String.valueOf(repeatsLeft));
		map.put("\\{done\\}", String.valueOf(repeated));
		map.put("\\{failed\\}", String.valueOf(failed));
		map.put("\\{lastpaid\\}", Utils.formatDateDiff(lastpaid));
		map.put("\\{nextpay\\}", Utils.formatDateDiff(nextpayment));
		return map;
	}
	
	/**
	 * @return  whether or not there is more to be paid
	 */
	public boolean moreToPay() {
		return (repeats == -1) || repeatsLeft > 0;
	}
	
	/**
	 * @return  whether or not should be paid now
	 */
	public boolean shouldBePaid() {
		return (lastpaid + delay) < System.currentTimeMillis();
	}
	
	/**
	 * @return  the UUID of the paying player
	 */
	public UUID getFromUUID() {
		return fromUUID;
	}
	
	/**
	 * @return  the UUID of the player getting paid
	 */
	public UUID getToUUID() {
		return toUUID;
	}
	
	/**
	 * @return  the amount paid
	 */
	public double getAmount() {
		return amount;
	}
	
	/**
	 * @return  the delay for this PeriodicPayment (in seconds)
	 */
	public long getDelay() {
		return delay;
	}
	
	/**
	 * @return  the number of repeats needed
	 */
	public long getRepeats() {
		return repeats;
	}
	
	/**
	 * @return  the number of repeats already done
	 */
	public long getRepeated() {
		return repeated;
	}
	
	/**
	 * @return  the number of repeats left to do
	 */
	public long getRepeatsLeft() {
		return repeatsLeft;
	}
	
	/**
	 * @return  the number of times failed
	 */
	public long getFailed() {
		return failed;
	}
	
	/**
	 * @return  the time the last successful payment was made (in milliseconds)
	 */
	public long getLastPaid() {
		return lastpaid;
	}
	
	/**
	 * @return  the time the next payment should be made (in milliseconds)
	 */
	public long getNextPayTime() {
		return nextpayment;
	}
	
	/**
	 * @return  the time the payment was started (in milliseconds)
	 */
	public long getStartTime() {
		return starttime;
	}
	
	/**
	 * 
	 */
	public String toString() {
		return Messages.PAYMENT_DESCRIPTION.get(getMap());
	}

}
