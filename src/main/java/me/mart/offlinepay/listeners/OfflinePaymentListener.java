package me.mart.offlinepay.listeners;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

import me.mart.offlinepay.OfflinePay;
import me.mart.offlinepay.events.OfflinePayEvent;
import me.mart.offlinepay.utils.Messages;

/**
 * Listener for the OfflinePayEvent
 * Cancels event if the player who pays does not have enough money
 * 
 * @author mart
 *
 */
public class OfflinePaymentListener implements Listener {
	
	/**
	 * Event listener.
	 * Cancels event if the player who pays does not enough money 
	 * 
	 * @param event  event being listened
	 */
	@EventHandler(priority=EventPriority.LOWEST)
	public void onOfflinePayment(OfflinePayEvent event) {
		boolean bFrom = OfflinePay.getOfflinePay().econ.hasAccount(event.getFromPlayer());
		boolean bTo = OfflinePay.getOfflinePay().econ.hasAccount(event.getToPlayer());
		if (!bFrom || !bTo) {
			event.setCancelled(true);
		}
		if (!event.isCancelled()) {
			if (!(OfflinePay.getOfflinePay().econ.has(event.getFromPlayer(), event.getAmount()))) { 
				// not enough money
				event.setCancelled(true);
			} 
		} else { // cancelled by something else
			Map<String, String> map = new HashMap<String, String>();
			map.put("\\{from\\}", event.getFromPlayer().getName());
			map.put("\\{to\\}", event.getToPlayer().getName());
			map.put("\\{amount\\}", String.valueOf(event.getAmount()));
			OfflinePay.LOGGER.info(Messages.PAYMENT_CANCELLED.get(map));
		}
	}
}
